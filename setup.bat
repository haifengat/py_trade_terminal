@echo off
: -D 生成目录方便程序更新
set python_home=D:\Anaconda37
set project_name=hf_terminal
pyinstaller --paths=%python_home%\Lib\site-packages\PyQt5\Qt\bin --paths=%python_home%\Lib\site-packages\PyQt5\Qt\plugins --paths=%python_home%\Lib\site-packages\py_ctp --clean -F -D -w hf_terminal.py
: 复制配置文件
copy config.json dist\%project_name%
: 复制界面美化文件
md dist\%project_name%\image
copy image\*.* dist\%project_name%\image
: 复制ctp相关文件
md dist\%project_name%\py_ctp\lib64
copy %python_home%\Lib\site-packages\py_ctp\lib64\*.dll dist\%project_name%\py_ctp\lib64 /Y
: 复制qt5相关文件
copy %python_home%\Lib\site-packages\PyQt5\Qt\bin\*.dll dist\%project_name%\PyQt5\Qt\bin /Y
: 使用融航接口复制文件 ** 覆盖后无法再用CTP接口 **
: copy Rohon\*.dll dist\%project_name%\py_ctp\lib64 /Y
pause

