# 交易终端_PY

## 项目介绍

交易终端python版 pyQT界面

## 软件架构

软件架构说明

## 安装教程

1. 下载最新py_ctp接口文档
    * [https://gitee.com/haifengat/hf_ctp_py_proxy/releases](https://gitee.com/haifengat/hf_ctp_py_proxy/releases)
2. 解压py_ctp接口文档,复制py_ctp目录到python安装目录下 Lib/site_packages
3. 环境
    * <font color='red'> 使用管理员安装 </font>
    * pip install PyQt5 PyQt5-tools
~~4. 配置config.json
    * ctp_dll_path
        * 接口的dll/so文件所在的目录~~

## 使用说明

1. setup.bat打包


2. 问题与解决

    * 遇到找不到sig错误
        * 在代码里面导入sip模块：from PyQt5 import sip
    * 不是有效有Win32程序
        * 复制thostxxx.dll到exe所在目录下

3. 附带文件

    * config.json
    * image\\\*.\*
    * ctp_trade.dll ctp_quote.dll
    * thostxxx.dll官方

4. color_log

    * [下载连接](https://gitee.com/haifengat/general_module/raw/master/color_log.py)
